""" This module contains the models for the application """
import datetime
from django.db import models
from django.utils import timezone
# Create your models here.
# That small bit of model code gives Django a lot of information. With it, Django is able to:

# 1.Create a database schema (CREATE TABLE statements) for this app.
# 2.Create a Python database-access API for accessing Question and Choice objects.

class Question( models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
        # return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published'
    
class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text= models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

# Migrations are very powerful and let you change your models over time, as you develop your project, without the need to delete your database or tables and make new ones - it specializes in upgrading your database live, without losing data. We’ll cover them in more depth in a later part of the tutorial, but for now, remember the three-step guide to making model changes:

# 1.Change your models (in models.py).
# 2.Run python manage.py makemigrations to create migrations for those changes
# 3. Run python manage.py migrate to apply those changes to the database.

# The reason that there are separate commands to make and apply migrations is because you’ll commit migrations
#  to your version control system and ship them with your app; they not only make your development easier,
#  they’re also useable by other developers and in production