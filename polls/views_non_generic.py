from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.urls import reverse
# from django.template import loader

from .models import Choice, Question

# Create your views here.

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    # template = loader.get_template('polls/index.html')
    context = {
        'latest_question_list' : latest_question_list
    }
    return render(request,'polls/index.html',context)
    # return HttpResponse(template.render(context, request))
    #return HttpResponse("Hello world, you are the poll's index.")

def detail(request, question_id):
    question = get_object_or_404(Question,pk=question_id)
    return render(request,"polls/detail.html",{"question":question})
    # There’s also a get_list_or_404() function, which works just as get_object_or_404() – except using filter() 
    # try:
    #      question = Question.objects.get(pk=question_id)
    # except Question.DoesNotExits:
    #     raise Http404("Question does not exist")
    # return render(request,"polls/detail.html",{"question":question})
    # return HttpResponse("You are looking at question %s."% question_id)

def results(request, question_id):
    question = get_object_or_404(Question,pk=question_id)
    return render(request,"polls/results.html",{"question":question})
    # response = "You are looking at the results of question %s."
    # return HttpResponse(response % question_id)

def vote(request, question_id):
    question = get_object_or_404(Question,pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except(KeyError,Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html',{ 'question':question, 'error_message':"You didn't select a choice."})
    else:
        selected_choice.votes +=1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results',args=(question.id,)))
    return HttpResponse("You're voting on question %s."% question_id)
